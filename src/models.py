from sqlalchemy import Column, Integer, String

from src.database import Base


class Recipe(Base):
    __tablename__ = "Recipe"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, unique=True)
    text = Column(String, index=True)
    ingredients = Column(String, index=True)
    cooking_time = Column(Integer, index=True)
    views = Column(Integer, index=True, default=0)
