from pydantic import BaseModel


class BaseRecipe(BaseModel):
    name: str
    cooking_time: int


class RecipeIn(BaseRecipe):
    text: str
    ingredients: str


class RecipeOutList(BaseRecipe):
    views: int

    class Config:
        orm_mode = True


class RecipeOutDetail(BaseRecipe):
    text: str
    ingredients: str
    views: int

    class Config:
        orm_mode = True
