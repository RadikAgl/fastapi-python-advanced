from typing import List, Sequence

from fastapi import FastAPI
from sqlalchemy.future import select

from src import models, schemas
from src.database import Base, engine, session

app = FastAPI()


@app.on_event("startup")
async def startapp() -> None:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown() -> None:
    await session.close()
    await engine.dispose()


@app.post("/recipes/", response_model=schemas.RecipeIn)
async def add_recipe(recipe: schemas.RecipeIn) -> models.Recipe:
    new_recipe = models.Recipe(**recipe.dict())
    async with session.begin():
        session.add(new_recipe)
    return new_recipe


@app.get("/recipes/", response_model=List[schemas.RecipeOutList])
async def get_recipes() -> Sequence[models.Recipe]:
    res = await session.execute(select(models.Recipe))
    return res.scalars().all()


@app.get("/recipes/{recipe_id}", response_model=schemas.RecipeOutDetail)
async def recipes(recipe_id: int) -> models.Recipe | None:
    recipe = await session.execute(
        select(models.Recipe).where(models.Recipe.id == recipe_id)
    )
    res = recipe.scalar()
    if res:
        if not res.views:
            res.views = 0
        res.views += 1
    await session.commit()
    return res


def sum_two(a: int, b: int) -> int:
    return a + b
